<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentController extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library(array('form_validation','session')); // load form lidation libaray & session library
        $this->load->helper(array('url','html','form'));  // load url,html,form helpers optional
         $this->load->model('Studentmodel');
    } 
	public function index()
	{
		$this->load->view('header.php');
		$this->load->view('registration.php');
		$this->load->view('footer.php');
	}

	public function get(){
		
		$data['info']= $this->Studentmodel->getInfo();
		$this->load->view('header.php');
		$this->load->view('student.php',$data);
		$this->load->view('footer.php');

	}

	public function view($id){

		$this->load->model('Studentmodel');
		$data['profile']= $this->Studentmodel->view($id);
		$this->load->view('header.php');
		$this->load->view('profile.php',$data);
		$this->load->view('footer.php');
	}
	public function delete($id){
		$this->Studentmodel->delete($id);
		$this->get();
	}


    public function create(){

        // set validation rules
    	
		$this->form_validation->set_rules('name', 'name', 'required|min_length[2]|max_length[15]');
		$this->form_validation->set_rules('s_id', 's_id', 'required|min_length[5]|max_length[15]');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email');
		$this->form_validation->set_rules('address', 'address', 'required');
		$this->form_validation->set_rules('gender', 'gender', 'required');

        // hold error messages in div
    	$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        // check for validation
    	if ($this->form_validation->run() == FALSE) {
    		
    		$this->index();

    	}else{
    		//put input data in array
    		$data = array(

			'name' => $this->input->post('name'),
			'std_id' => $this->input->post('s_id'),
			'email' => $this->input->post('email'),
			'address' => $this->input->post('address'),
			'gender' => $this->input->post('gender')
			);
			//insert data into databse
 			$this->Studentmodel->create($data);
 			//print success messege
    		$this->session->set_flashdata('student', 'form submitted successfully');
    		$this->index();
    	}

    }
}
