
<div class="container">
  <h2>Student List</h2>          
  <table class="table table-hover ">
    <thead>
      <tr>
        <th>Name</th>
        <th>ID</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    	<?php
    	foreach($info as $row){
		echo "<tr >
        <td  class='name'>".$row->name."
        <div class='info' style='display:none;'>
        <span>Email:".$row->email."</span><br />
        <span>Address: ".$row->address."</span><br />";
        if($row->gender==0){
                      echo 'Gender:Male';
                    }
                    else{
                      echo 'Gender:Female';
                    } 
        echo "
        <div>
        </td>
        <td>".$row->std_id."
        </td>
        <td><a href='view/".$row->id."' class='btn btn-primary btn-xs'>
          <span class='glyphicon glyphicon-eye-open'></span>View 
        </a><a href='delete/".$row->id."' class='btn btn-danger btn-xs'>
          <span class='glyphicon glyphicon-remove'></span>Delete 
        </a></td>
      	</tr>";

  	} ?>
    </tbody>
  </table>
  <a href='<?php echo site_url(); ?>/StudentController' class='btn btn-success '>
          <span ></span>Registration 
        </a>

</div>
   <script type="text/javascript">
   $(document).ready(function(){
    $(".name").mouseover(function(){
          $(".info",this).show();
          
    });
$(".name").mouseout(function(){
        $(".info").css("display", "none");
    });
});
</script>
</body>

</html>
