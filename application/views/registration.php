<!DOCTYPE html>
<html lang="en">
<head>


    <meta name="viewport" content="width=device-width"> 
    <link href="<?php echo base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/css/bootstrap-responsive.min.css" rel="stylesheet">

</head>
<body>
    <div class="container">
        <div class="col-md-8">
            <?php if(validation_errors()) { ?>
            <div class="alert alert-warning">
                <?php echo validation_errors(); ?>
            </div>
            <?php } ?>


            <?php if($this->session->flashdata('student')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('student'); ?>
            </div>
            <?php } ?>
            <form id="registration-form" class="form-horizontal" role="form" method="post" action="<?php echo site_url();?>/StudentController/create">

                <h1>Sample Student Registration form</h1>
                <br/>

                <div class="form-group">
                    <label class="control-label" for="name">Name</label>
                    <div class="controls">
                        <input type="text" class="form-control required" name="name" id="name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="email">Email</label>
                    <div class="controls">
                        <input type="text" class="form-control required" name="email" id="email">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="s_id">Student ID</label>
                    <div class="controls">
                        <input type="text" class="form-control required" name="s_id" id="s_id">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="message">Address</label>
                    <div class="controls">
                        <textarea class="form-control required" name="address" id="address" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="message">Gender</label>
                    <div class="controls ">
                        <label ><input type="radio" name="gender" value="0" >Male</label>
                        <label class="radio-inline"><input type="radio" name="gender" value="1">Female</label>
                    </div>
                </div>

                <div class="form-group  center">
                    <button type="submit" class="btn btn-success btn-large">Register</button>
                    <button type="reset" class="btn">Cancel</button>
                </div>

            </form>
        </div>
    </div>
    <!-- .container --> 
   
    <script type="text/javascript">
   $(document).ready(function() {
   $("#registration-form").validate();
   });
</script>

</body>
</html>
