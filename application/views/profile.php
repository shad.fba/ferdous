<div class="container">
<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
			
			
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Student Profile</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class=" col-md-9 col-lg-9 "> 
							<table class="table">
								<tbody>
									<?php
									foreach($profile as $row){ ?>
									<tr>
										<td>Name:</td>
										<td><?php echo $row->name; ?></td>
									</tr>
									<tr>
										<td>ID:</td>
										<td><?php echo $row->std_id; ?></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><?php echo $row->email; ?></td>
									</tr>
									<tr>
										<td>Gender</td>
										<td><?php  if($row->gender==0){
											echo "Male";
										}
										else{
											echo "Female";
										}
										?></td>
									</tr>
									<tr>
										<td>Address</td>
										<td><?php echo $row->address; ?></td>
									</tr>
									
									<?php }
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				
			</div>
			<a href='<?php echo site_url(); ?>/StudentController/get' class='btn btn-success btn-l'>
				<span ></span>Back 
			</a>
		</div>
</div>

	